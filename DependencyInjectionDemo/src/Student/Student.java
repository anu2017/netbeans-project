/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Student;

import java.util.Objects;

/**
 *
 * @author ICIT
 */
public class Student {
    String name;
    int roll;
    int marks;
    int age;

    public Student(String name, int roll, int marks, int age) {
        this.name = name;
        this.roll = roll;
        this.marks = marks;
        this.age = age;
    }

    public Student() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.name);
        hash = 23 * hash + this.roll;
        hash = 23 * hash + this.marks;
        hash = 23 * hash + this.age;
        return hash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoll() {
        return roll;
    }

    public void setRoll(int roll) {
        this.roll = roll;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.roll != other.roll) {
            return false;
        }
        if (this.marks != other.marks) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", roll=" + roll + ", marks=" + marks + ", age=" + age + '}';
    }
    
    
}
