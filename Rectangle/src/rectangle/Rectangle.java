/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rectangle;


public class Rectangle {

    double length,breadth;
    Rectangle()
    {
        this.length = 1;
        this.breadth = 1;
    }
    Rectangle(double a,double b)
    {
        this.length = a;
        this.breadth = b;
    }
    void setDimension(int l,int b)
    {
        this.length = l;
        this.breadth = b;
    }
    double area(int l,int b)
    {
        double area_r = l * b;
        return area_r;
    }
    double area()
    {
        double area_r = this.length * this.breadth;
        return area_r;
    }
    double perimeter()
    {
        double peri_r = 2*(this.length+this.breadth);
        return peri_r;
    }
    
    public static void main(String[] args) 
    {
        Rectangle r1 = new Rectangle();
        System.out.println("Length :: " + r1.length + " " + "Breadth :: " + r1.breadth + " " + "Area :: " + r1.area());
        r1.setDimension(2, 2);
        System.out.println("Length :: " + r1.length + " " + "Breadth :: " + r1.breadth + " " + "Perimeter :: " + r1.perimeter());
        Rectangle r2 = new Rectangle(3,4);
        System.out.println("Length :: " + r2.length + " " + "Breadth :: " + r2.breadth + " " + "Area :: " + r2.area());
    }
    
}
