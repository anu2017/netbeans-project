package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
    @RequestMapping("/welcome")
    public ModelAndView index()
    {
        return new ModelAndView("index");
    }
    
    @RequestMapping("/hi")
    public ModelAndView hiFun()
    {
        return new ModelAndView("test");
    }
}
