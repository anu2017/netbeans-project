<%-- 
    Document   : login
    Created on : Nov 4, 2017, 6:27:13 PM
    Author     : ICIT
--%>

<%

    if (session.getAttribute("admin_name") != null && session.getAttribute("admin_name").equals("admin")) {
        response.sendRedirect("dashboard.htm");

    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            *{
                color : black;
                background-color: bisque;
            }

            #parentdiv {
                display: table;
                width: 16.5em;
                margin: 0 auto;
            }
            #form_login {
                display: table-cell;
                text-align: center;
                vertical-align: middle;
                border : solid 1px;
                
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student Database-Spring Framework</h1><br><br>
        <h3 text-align: center style ="color:red"><%=request.getParameter("errmsg") == null ? "" : request.getParameter("errmsg")%></h3><br>
        <div id="parentdiv">
            <form id ="form_login" action ="loginaction.htm" method ="post">
                <table border="1">
                    <tr>
                        <td>
                            User ID :   
                        </td>
                        <td>
                            <input type ="text" name = "name" value ="" autofocus/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password : 
                        </td>

                        <td>
                            <input type="password" name ="password" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Login">
                        </td>
                    </tr>
                </table>
            </form>

        </div>
    </body>
</html>
