<%-- 
    Document   : dashboard
    Created on : Nov 4, 2017, 6:30:30 PM
    Author     : ICIT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student Dashboard Page</h1>
        <h3>Add/Update/Delete Employees</h3>

        <table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
            </tr>
            <c:forEach var="x" items="${students}">

                <tr>

                    <td>
                        ${x.id} <br/>
                    </td>
                    <td>
                        ${x.name} <br/>
                    </td>
                    <td>
                        ${x.age} <br/>
                    </td>

                </tr>
            </c:forEach>
        </table>


        <br><br>
        <a href="addstudent.htm"> Add</a>
        <a href="logoutaction.htm"> Logout</a>




    </body>
</html>
