package com.controller;

import com.Hibernate.HibernateCoreDemo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Student;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        return new ModelAndView("/admin/login");
    }
    
    @RequestMapping(value = "/addstudent", method = RequestMethod.GET)
    public ModelAndView addstudent() {
        return new ModelAndView("/admin/addstudent");

    }
    
    @RequestMapping(value = "/addstudentaction", method = RequestMethod.POST)
    public ModelAndView addstudentaction(HttpServletResponse response, HttpSession session, @RequestParam("studentname") String student_name, @RequestParam("studentage") String student_age) {
        System.out.println("Name " + student_name + " Age " + student_age + " added");
        HibernateCoreDemo hcd = new HibernateCoreDemo();
        hcd.setList(student_name, Integer.parseInt(student_age));
        return new ModelAndView("/admin/addstudent");

    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView logaction(HttpSession session) {
        ModelAndView mv = new ModelAndView("/admin/dashboard");
        
//        List list = new ArrayList();
//        list.add("Apple");
//        list.add("Banana");
//        list.add("Mango");
//        mv.addObject("fruits", list);
        
        
        List<Student> ls = new ArrayList<Student>();
        HibernateCoreDemo hcd = new HibernateCoreDemo();
        ls=hcd.getList();
        mv.addObject("students",ls);
        
        
        return (mv);

    }

    @RequestMapping(value = "/loginaction", method = RequestMethod.POST)
    public void loginaction(HttpServletResponse response, HttpSession session, @RequestParam("name") String admin_name, @RequestParam("password") String admin_password) throws IOException {
        System.out.println(admin_name);
        System.out.println(admin_password);
        if (admin_name.equals("admin") && admin_password.equals("@dmin")) {
            session.setAttribute("admin_name", admin_name);
            response.sendRedirect("dashboard.htm");
        } else {
            response.sendRedirect("login.htm?errmsg=Username or Password is incorrect");
        }

    }

    @RequestMapping(value = "/logoutaction", method = RequestMethod.GET)
    public void logoutaction(HttpServletResponse response, HttpSession session) throws IOException {
        session.invalidate();
        response.sendRedirect("login.htm");

    }
    
    
}
