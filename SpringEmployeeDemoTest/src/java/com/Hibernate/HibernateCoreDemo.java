package com.Hibernate;

import java.util.ArrayList;
import java.util.List;
import model.Student;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateCoreDemo {
    //public static void main(String[] args)
    public List getList()
    {
        
        System.out.println("Database Connection");
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
        SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
        Session session = factory.openSession();
        
        //List list = session.createQuery("FROM Student").list();
        
        String sql = "select * from student";
        SQLQuery query = session.createSQLQuery(sql);
        query.addEntity(Student.class);
        List list = query.list();
        System.out.println(list);
        
        return list;
        
        
    }
    public void setList(String student_name,int student_age)
    {
        Student s = new Student(Integer.MIN_VALUE,student_name,student_age);
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
        SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
        Session session = factory.openSession();
        Transaction t = session.beginTransaction();
        session.save(s);
        t.commit();
        
//        String sql = "INSERT INTO `student`(`id`, `name`, `age`) VALUES (NULL,student_name,student_age)";
//        SQLQuery insquery = session.
    }
    
}
