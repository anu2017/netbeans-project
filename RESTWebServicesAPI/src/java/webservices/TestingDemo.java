/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author ICIT
 */
@Path("api")                    //Main API
public class TestingDemo {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TestingDemo
     */
    public TestingDemo() {
    }

    /**
     * Retrieves representation of an instance of webservices.TestingDemo
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/plain")@Path("/method1")
    public String getText(@QueryParam("id1") String firstname,@QueryParam("id2") String lastname) {
        return "MyFirstWebServvice"+firstname+" "+lastname;
    }
    @GET                    //Gets value of paramters from URL
    @Produces("text/plain") //Produces body of webpage in plain text
    @Path("/method2")       //Path in URL under main path API. Method2 is subAPI
    public String getText2(@DefaultValue("abc") @QueryParam("id1") String firstname,@QueryParam(value="id2") String lastname) {
        return "MyFirstWebServvice-From Method2"+firstname+" "+lastname;
    }
    
    @GET                    //Gets value of paramters from URL
    @Produces("text/plain") //Produces body of webpage in plain text
    @Path("/method3")       //Path in URL under main path API. Method2 is subAPI
    public String getText2(@DefaultValue("1") @QueryParam("num") int n1) {
        int square = n1*n1;
        int cube = square*n1;
        return "{ \"square\" : " + "\"" + square + "\"" + "," + " \"cube\" : " + "\"" + cube + "\"" + " }";
    }
    
    @POST
    @Produces("text/plain")
    @Path("/method4")
    public Response postMsg1(@DefaultValue("Default Value here") @QueryParam("msg1") String msg) {
        String output = "POST:Says " + msg;
        return Response.status(200).entity(output).build();
    }
    

    /**
     * PUT method for updating or creating an instance of TestingDemo
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/plain")
    public void putText(String content) {
    }
}
