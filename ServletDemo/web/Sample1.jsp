<%-- 
    Document   : Sample1
    Created on : Sep 10, 2017, 1:19:17 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            int x = 2;
            int y = 9;
            
        %>
        
        <h1>
            <%=x%>
            <%=y%><br>
            <%if(x+y > 20)
            {
              out.println(x+y);  
            }
            else
            {
              out.println("Less than 20");  
            }
            %>
        </h1>
                
    </body>
</html>
