<%-- 
    Document   : DeclarationTagDemo
    Created on : Sep 10, 2017, 1:51:35 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%!
        int cube(int n)
        {
            return n*n*n;
        }
        %>
        <%= "Cube of 3 is " + cube(3) %>
    </body>
</html>
