<%-- 
    Document   : index
    Created on : Sep 10, 2017, 12:13:35 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=request.getAttribute("msg")%></title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p>Sum of Below Numbers</p>
        <form action ="MyMath" method = "post">
        Enter Number1
        <input type ="text" name="num1" value = <%=request.getAttribute("n1")%> ><br><br>
        Enter Number2
        <input type ="text" name="num2" value = <%=request.getParameter("num2")%> ><br><br>
        
        <%if(request.getAttribute("Sum") != null)
          {
            out.println("Sum of Numbers are " + request.getAttribute("Sum") );
          }%>  <br><br>
        
        <input type ="submit" value="Submit"><br>
        </form>
        
    </body>
</html>
