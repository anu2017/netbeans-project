<%-- 
    Document   : TestNoSpring
    Created on : Sep 17, 2017, 12:09:32 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World...This is a JSP Page not running via Spring Framework!</h1>
    </body>
</html>
