/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.interfaces;

import java.util.List;
import model.pojo.Student;

/**
 *
 * @author ICIT
 */
public interface StudentDAO {
    public List selectAllStudents();
    public List selectStudent(int id);
    public void insertStudent(Student s);
    
}
