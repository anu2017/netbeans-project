/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.interfaces;

import java.util.List;
import model.pojo.Student;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ICIT
 */
public class StudentDAOImpl implements StudentDAO{

    @Override
    public List selectAllStudents() {
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
        SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
        Session session = factory.openSession();
        
        //List list = session.createQuery("FROM Student").list();
        
        
        String sql = "select * from student";
        SQLQuery query = session.createSQLQuery(sql);
        query.addEntity(Student.class);
        List list = query.list();
        return list;
    }

    @Override
    public List selectStudent(int id) {
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
        SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
        Session session = factory.openSession();
        
        String sql = "select * from student where id = " + id;
        SQLQuery query = session.createSQLQuery(sql);
        query.addEntity(Student.class);
        List list = query.list();
        return list;
    }

    @Override
    public void insertStudent(Student s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
