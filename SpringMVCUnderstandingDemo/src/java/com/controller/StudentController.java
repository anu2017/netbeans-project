/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import model.dao.interfaces.StudentDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/student")
public class StudentController {
    @RequestMapping("/test")
    public ModelAndView method() {
        
        ModelAndView mv1 = new ModelAndView("student/test");
        
        StudentDAOImpl sdi = new StudentDAOImpl();
        
        mv1.addObject("StudentList", sdi.selectAllStudents());
        
        return mv1;
    }
    
    @RequestMapping("/record")
    public ModelAndView getSingleStudentRecord(@RequestParam("id") int student_id) {
        
        ModelAndView mv1 = new ModelAndView("student/studentdata");
        
        System.out.println(student_id + ">>>>>>>>>>>");
        
        StudentDAOImpl sdi = new StudentDAOImpl();
        
        mv1.addObject("StudentList", sdi.selectStudent(student_id));
        
        return mv1;
    }
    
}
