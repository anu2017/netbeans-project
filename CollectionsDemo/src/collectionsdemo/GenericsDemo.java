/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsdemo;

import java.util.ArrayList;


public class GenericsDemo {
    public static void main(String[] args)
    {
        //ArrayList <String> fruits = new ArrayList<String>();
        ArrayList <String> fruits = new ArrayList(); //1.7+
        fruits.add("Apple");
        //fruits.add(1);
        System.out.println(fruits);
    }
}
