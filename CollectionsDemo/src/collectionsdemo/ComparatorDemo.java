/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsdemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

class Student1
{
    String name;
    int age;
    int roll;
    int marks;
    
    Student1()
    {
        name = "Ram";
        age=50;
        roll=12;
        marks=40;
    }
    Student1(String n,int a,int r,int m)
    {
        this.name = n;
        this.age=a;
        this.roll=r;
        this.marks=m;
    }

    @Override
    public String toString() {
        return "Student1{" + "name=" + name + ", age=" + age + ", roll=" + roll + ", marks=" + marks + '}';
    }

    
}

class AgeComparator implements Comparator<Student1>
{
        //int roll;
        @Override
        public int compare(Student1 s1,Student1 s2)
        {
          if(s1.age==s2.age)
            {
                return 0;
            }
            else if(s1.age > s2.age)
            {
               return 1;
            }
            else
             {
                return -1;
             }  
          }
        
}

class NameComparator implements Comparator<Student1>
{
        //int roll;
        @Override
        public int compare(Student1 s1,Student1 s2)
        {
            return(s1.name.compareTo(s2.name));
            
        }
        
}
public class ComparatorDemo {
    public static void main(String[] args)
    {
        Student1 s1 = new Student1("Anu",50,3,30);
        Student1 s2 = new Student1("Ram",40,2,30);
        Student1 s3 = new Student1("Sitaa",10,1,30);
        
        ArrayList <Student1> c1 = new ArrayList();
        c1.add(s1);
        c1.add(s2);
        c1.add(s3);
        
        Iterator ir = c1.iterator();
                
        while(ir.hasNext())
        {
           
            System.out.println(ir.next());
        }
        
        
        Collections.sort(c1, new AgeComparator());
        
        System.out.println("After Sorting with age parameter \n");
        
        Iterator ir1 = c1.iterator();
                
        while(ir1.hasNext())
        {
           
            System.out.println(ir1.next());
        }
        Collections.sort(c1, new NameComparator());
        System.out.println("After Sorting with age parameter \n");
        
        Iterator ir2 = c1.iterator();
                
        while(ir2.hasNext())
        {
           
            System.out.println(ir2.next());
        }
    }
}
