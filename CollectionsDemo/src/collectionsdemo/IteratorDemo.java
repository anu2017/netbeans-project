/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsdemo;

import java.util.ArrayList;
import java.util.Iterator;


public class IteratorDemo {
    public static void main(String[] args)
    {
        ArrayList c = new ArrayList();
        c.add("Jan");
        c.add("Feb");
        c.add("Mar");
        
       // Iterator ir = c.iterator();
//        while(ir.hasNext())
//        {
//            String s = (String)(ir.next());
//            if(s.equals("Mar"))
//            {
//                ir.remove();
//            }
//            System.out.println(s);
//            
//            
//        }
//        System.out.println(c);
//    }
        for(Iterator ir = c.iterator(); ir.hasNext(); )
        {
            String s = (String)(ir.next());
            System.out.println(s);
            
        }
    }
}
