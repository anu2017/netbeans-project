/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsdemo;

import java.util.ArrayList;
import java.util.Collection;


public class CollectionsDemo {

    
    public static void main(String[] args) {
        int[] a = new int[3];
        a[0]=12;
        a[1]=13;
        a[2]=14;
        System.out.println("Array is " + a[0]+ " " + a[1]+ " " + a[2]);
       
        Object[] ar = new Object[3];
        ar[0]="Anu";
        ar[1]=80;
        ar[2]=9.7;
        System.out.println("Object is " + ar[0]+ " " + ar[1]+ " " + ar[2]);
        
        ArrayList c = new ArrayList();
        c.add(a);
        c.add(ar);
        System.out.println("Collection is" + c.get(0) + " " + c.get(1));
        
        ArrayList fruits = new ArrayList();
        fruits.add("Oraange");
        fruits.add("Banana");
        fruits.add("Apple");
        fruits.add("Litchi");
        System.out.println("Fruits are " + fruits);
        fruits.sort(null);
        System.out.println("Fruits are " + fruits);
        fruits.remove("Oraange");
        System.out.println("Fruits are " + fruits);
        fruits.remove(2);
        System.out.println("Fruits are " + fruits);
        fruits.removeAll(fruits);
        System.out.println("Fruits are " + fruits);
        
        
        ArrayList fruits1 = new ArrayList();
        fruits1.add("Litchi");
        fruits1.add("Apple");
        fruits1.add("Mango");
        fruits1.add("Guava");
        ArrayList fruits2 = new ArrayList();
        fruits2.add("Oraange");
        fruits2.add("Banana");
        fruits2.add("Apple");
        fruits2.add("Litchi");
        System.out.println("Fruit Set 1 " + fruits1);        
        System.out.println("Fruit Set 2 " + fruits2);
        fruits1.removeAll(fruits2);
        System.out.println("Post removeAll \n Fruit Set 1 " + fruits1);        
        System.out.println("Fruit Set 2 " + fruits2);
        fruits1.add("Litchi");
        fruits1.add("Apple");
        fruits1.retainAll(fruits2);
        System.out.println("Post RetainAll \n Fruit Set 1 " + fruits1);        
        System.out.println("Fruit Set 2 " + fruits2);
        
        System.out.println("Size is " + fruits2.size());
        if(fruits2.isEmpty())
        {
            System.out.println("Empty fruits2");
        }
        else
        {
            System.out.println("Non empty");
        }
        System.out.println("Fruit Set 1 " + fruits1);        
        System.out.println("Fruit Set 2 " + fruits2);
        fruits2.remove(0);
        fruits2.remove(0);
        System.out.println("Fruit Set 1 " + fruits1);        
        System.out.println("Fruit Set 2 " + fruits2);
        fruits1.sort(null);
        fruits2.sort(null);
        System.out.println("Fruit Set 1 " + fruits1);        
        System.out.println("Fruit Set 2 " + fruits2);
        if(fruits1.equals(fruits2))
        {
            System.out.println("Equal");
        }
        else
        {
            System.out.println("Not Equal");

        }
        if(fruits2.contains("Apple"))
        {
            System.out.println("Contains Apple");
        }
        
        //toArray method
        String[] fr = new String[2];
        fruits2.toArray(fr);
        System.out.println(fr[0]);
        
    }
    
}
