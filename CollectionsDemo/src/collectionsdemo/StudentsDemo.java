/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsdemo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

class Student implements Comparable<Student>
{
    String name;
    int age;
    int roll;
    int marks;
    
    Student()
    {
        name = "Ram";
        age=50;
        roll=12;
        marks=40;
    }
    Student(String n,int a,int r,int m)
    {
        this.name = n;
        this.age=a;
        this.roll=r;
        this.marks=m;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", age=" + age + ", roll=" + roll + ", marks=" + marks + '}';
    }
    
    @Override
    public int compareTo(Student s)
    {
        if(this.roll==s.roll)
        {
            return 0;
        }
        else if(this.roll > s.roll)
        {
            return 1;
        }
        else
        {
            return -1;
        }
            
    }
    
    
    
}

public class StudentsDemo {
    public static void main(String[] args)
    {
        Student s1 = new Student("Anu",50,3,30);
        Student s2 = new Student("Ram",40,2,30);
        Student s3 = new Student("Sitaa",50,1,30);
        
        ArrayList <Student> c = new ArrayList();
        c.add(s1);
        c.add(s2);
        c.add(s3);
        
        Iterator ir = c.iterator();
                
        while(ir.hasNext())
        {
           
            System.out.println(ir.next());
        }
        
        
        Collections.sort(c);
        
        System.out.println("After Sorting \n");
        Iterator ir1 = c.iterator();
                
        while(ir1.hasNext())
        {
           
            System.out.println(ir1.next());
        }
        
        
    }
}
