/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filehandling;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


public class FileHandling {

   
    public static void main(String[] args) throws Exception{
        File file = new File("text_anu.txt");
        //FileOutputStream fos = new FileOutputStream(file);
        //DataOutputStream dos = new DataOutputStream(fos);
        
        //dos.writeUTF("Line 2 here");
        
        //dos.close();
        //fos.close();
        
        FileInputStream fis = new FileInputStream(file);
        DataInputStream dis = new DataInputStream(fis);
        
        String file_content = dis.readUTF();
        
        System.out.println("Live Stream :: " + file_content);
        
        dis.close();
        fis.close();
        
    }
    
}
