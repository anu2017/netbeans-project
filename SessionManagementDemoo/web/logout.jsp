<%-- 
    Document   : logout
    Created on : Sep 13, 2017, 10:13:35 AM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            session.invalidate();
            response.sendRedirect("index.jsp");
        %>    
    </body>
</html>
