/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcmysqldemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author ICIT
 */
public class JDBCMySQLDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/icit", "root", "");
//here sonoo is database name, root is username and password  
            //int opt = 1;
            System.out.println("Enter option. Enter 0 to exit \n 1.Select \n 2.Insert \n 3.Update \n 4.Delete");
            
            int n = 0;
            do
            {
                
                switch(n)
                {
                    case 1 :Statement stmt1 = con.createStatement();
                            ResultSet rs1 = stmt1.executeQuery("select * from student");
                            while (rs1.next()) {
                                System.out.println(rs1.getInt(1) + "  " + rs1.getString(2) + "  " + rs1.getString(3));
                             }
                            break;
                    case 2 :Statement stmt2 = con.createStatement();
                            stmt2.execute("INSERT INTO `student` (`id`, `name`, `age`) VALUES (NULL, 'raghav', '40');");
                            break;
                    case 3 :Statement stmt3 = con.createStatement();
                            boolean b = stmt3.execute("UPDATE `student` SET `age`=\"25\" WHERE `id` = 2");
                            System.out.println("Value of statement 3 " + b);
                            break;
                        
                    case 4 :Statement stmt4 = con.createStatement();
                            stmt4.execute("DELETE `student` WHERE `id` = 1;");
                            break;
                    default :System.out.println("Enter option. Enter 0 to exit \n 1.Select \n 2.Insert \n 3.Update \n 4.Delete"); 
                }
                Scanner sc = new Scanner(System.in);
                n = sc.nextInt();
            }while(n!=0);
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
