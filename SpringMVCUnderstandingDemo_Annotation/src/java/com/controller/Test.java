/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/abc")
public class Test {
    @RequestMapping("/xyz")
    public ModelAndView method()
    {
        
        ModelAndView mv = new ModelAndView("pqr");
        
        List list = new ArrayList();
        
        list.add("Apple");
        list.add("Banana");
        list.add("Mango");
        mv.addObject("fruits", list);
        return (mv);
    }
}
