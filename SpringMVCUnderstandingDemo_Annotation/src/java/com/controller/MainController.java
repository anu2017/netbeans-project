
package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/main")
public class MainController {
    
    @RequestMapping("/test")
    public ModelAndView methood1() {
        return new ModelAndView("/main/test");
    }
    
    @RequestMapping("/test2")
    public ModelAndView methood2() {
        return new ModelAndView("/main/test2");
    }
    
}
