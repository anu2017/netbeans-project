
package hibernatecoredemo;

import java.util.List;
import model.Student;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateCoreDemo {


    public static void main(String[] args) {
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
        SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
        Session session = factory.openSession();
        
        //List list = session.createQuery("FROM Student").list();
        
        
        String sql = "select * from student LIMIT 2";
        SQLQuery query = session.createSQLQuery(sql);
        query.addEntity(Student.class);
        List list = query.list();
        System.out.println(list);
        
//        Student s = new Student("Rajesh", 30);
//        Transaction t = session.beginTransaction();
        
//        session.save(s);
//        t.commit();
        
        
        
    }
    
}
