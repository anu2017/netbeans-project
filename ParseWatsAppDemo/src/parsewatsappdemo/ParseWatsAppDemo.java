package parsewatsappdemo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class ParseWatsAppDemo implements Serializable {

    public static void main(String[] args) throws FileNotFoundException, IOException {
//        // Old way of reading file and printing it line by line
//        File f = new File("Chat.txt");
//        FileInputStream fis = new FileInputStream(f);
//        DataInputStream dis = new DataInputStream(fis);
//        String str;
//
//        System.out.println("Chat Stream :: ");
//        while ((str = dis.readLine()) != null && str.length() != 0) {
//            System.out.println(str);
//        }
//Free Resources
//        dis.close();
//        fis.close();

        //*******Java 1.8 Way of reading file*********
        String fileName = "Chat.txt";

        //read file into stream, try-with-resources
//        try (Stream<String> lines = Files.lines(Paths.get(fileName))) 
//        {
//
//            lines.forEach(System.out::println);
//        } 
//        catch (IOException e) {
//            e.printStackTrace();
//        }
        
        HashMap hm = new HashMap();
        //HashMap hm_1 = new HashMap();
        
        Stream<String> lines = Files.lines(Paths.get(fileName));
        for (Iterator i1 = lines.iterator(); i1.hasNext();) {
            String s1 = (String) i1.next();
            System.out.println(s1);

            Pattern pattern = Pattern.compile("(\\d{2}/\\d{2}/\\d{2}).*-(.*):.*");

            Matcher matcher = pattern.matcher(s1);
            
            if (matcher.matches()) {
                System.out.println("Date: " + matcher.group(1));
                System.out.println("Name: " + matcher.group(2).trim());
                
                hm.put(matcher.group(1), matcher.group(2).trim());
            }
        }
        
        System.out.println("HashMap :: " + hm);

//        List fruits = new ArrayList();
//        fruits.add("apple");
//        fruits.add("Banana");

        //fruits.forEach(System.out::println);
//        for (Iterator i = fruits.iterator(); i.hasNext();) {
//            String s = (String) i.next();
//            System.out.println(s);
//        }
    }

}
