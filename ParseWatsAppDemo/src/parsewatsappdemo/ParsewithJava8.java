package parsewatsappdemo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class ParsewithJava8 {
 public static void main(String[] args) {

        Map<String, Map> chatcat = new HashMap<>();
        String fileName = "whatsappchat.txt";

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            for (Iterator i = stream.iterator(); i.hasNext();) {
                String str = (String) i.next();

                Pattern p = Pattern.compile("^(.*),.*\\s-\\s*([^:]+):.*$");
                Matcher m = p.matcher(str);

                if (m.matches()) {
                    String date_key = m.group(1);
                    String chat_log = m.group(2);

                    if (chatcat.get(date_key) != null && !chatcat.get(date_key).containsKey(chat_log)) {
                        Map<String, Integer> map = chatcat.get(date_key);
                        map.put(chat_log, 1);
                        chatcat.put(date_key, map);
                    } else if (chatcat.get(date_key) != null && chatcat.get(date_key).containsKey(chat_log)) {
                        Map<String, Integer> map = chatcat.get(date_key);
                        int cnt = (int) map.get(chat_log);
                        map.put(chat_log, ++cnt);
                        chatcat.put(date_key, map);
                    }

                    if (!chatcat.containsKey(date_key)) {
                        Map<String, Integer> map = new HashMap<>();
                        map.put(chat_log, 1);
                        chatcat.put(date_key, map);
                    }
                }
            }

            chatcat.forEach((key, value) -> {
                System.out.println("Date : " + key + " Chatlog : " + value);
            });

        } catch (IOException e) {
            //e.printStackTrace();
			System.out.println("Something went wrong");
        }
    }
}
