/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

@Controller
@RequestMapping("/employee")
public class Employee {
    @RequestMapping("/show_employee")
    public ModelAndView showdata(){
    ModelAndView mv = new ModelAndView("/employee/show_employee");
    return mv;
        
    }
    
    @RequestMapping("/test")
     public ModelAndView show_data(){
    ModelAndView mv1 = new ModelAndView("/employee/test");
    return mv1;
        
    }
}
